import os
import time
from random import randint

pron = ["I ", "You ", "We ", "They ", "He ", "She ", "It "]


def tobe_table():
    right_ans, all_ans = 0, 0
    while True:
        num_tab = randint(1, 9)
        num_pr = randint(0, 6)
        p = pron[num_pr]
        all_ans += 1

        # Меню
        os.system('cls||clear')
        print("Местоимение - " + p)

        if num_tab == 1:
            print("**Вопрос в будущем времени**")
            input('Узнать ответ')
            print("Will " + p + "be ?")

        if num_tab == 2:
            print("**Утверждение в будущем времени**")
            input('Узнать ответ')
            print(p + "will be")

        if num_tab == 3:
            print("**Отрицание в будущем времени**")
            input('Узнать ответ')
            print(p + "will not be")

        if num_tab == 4:
            print("**Вопрос в настоящем времени**")
            input('Узнать ответ')
            if p == "I ":
                print("Am" + p + "?")
            elif p == "He " or p == "She " or p == "It ":
                print("Is " + p + "?")
            else:
                print("Are " + p + "?")

        if num_tab == 5:
            print("**Утверждение в настоящем времени**")
            input('Узнать ответ')
            if p == "I ":
                print(p + "am")
            elif p == "He " or p == "She " or p == "It ":
                print(p + "is")
            else:
                print(p + "are")

        if num_tab == 6:
            print("**Отрицание в настоящем времени**")
            input('Узнать ответ')
            if p == "I ":
                print(p + "am not")
            elif p == "He " or p == "She " or p == "It ":
                print(p + "is not")
            else:
                print(p + "are not")

        if num_tab == 7:
            print("**Вопрос в прошедшем времени**")
            input('Узнать ответ')
            if p == "I " or p == "He " or p == "She " or p == "It ":
                print("Was " + p + "?")
            else:
                print("Were " + p + "?")

        if num_tab == 8:
            print("**Утверждение в прошедшем времени**")
            input('Узнать ответ')
            if p == "I " or p == "He " or p == "She " or p == "It ":
                print(p + "was")
            else:
                print(p + "were")

        if num_tab == 9:
            print("**Отрицание в прошедшем времени**")
            input('Узнать ответ')
            if p == "I " or p == "He " or p == "She " or p == "It ":
                print(p + "was not")
            else:
                print(p + "were not")

        print("Число - " + str(num_tab) + "\n\n" + "Ответил верно?")
        a = input('''Да "+"| Нет "enter"| Выход "q" ''')
        if a == "q":
            print("Правильных ответов - " + str(right_ans) + " из - " + str(all_ans))
            time.sleep(1.5)
            break
        elif a == "+":
            right_ans += 1
            print("Круто")
            time.sleep(0.7)
    os.system('cls||clear')
