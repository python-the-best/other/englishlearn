import os
import time
from random import randint

# Местоимения
pronouns = [["I ", "You ", "We ", "They ", "He ", "She "],
            ["Я ", "Ты ", "Мы ", "Они ", "Он ", "Она "]]
# Глаголы
verbs = [[" love", " like", " close", " use", " cope", " say", " know", " take", " see", " work"],
         [" loved", " liked", " closed", " used", " coped", " said", " knew", " took", " saw", " worked"]]


def declension_russ(man, ver, pron):
    """Скланения глаголов на русском языке
    :param man: main random num for table
    :param ver: random num for list verbs
    :param pron: random num for list pronouns
    :return: verb"""
    # Будущее
    if man == 1 or man == 2 or man == 3:
        if ver == 0: verb = " любить"
        if ver == 1: verb = " нравится"
        if ver == 2:
            if pron == 0 or pron == 1 or pron == 4: verb = " закрыт"
            if pron == 2 or pron == 3: verb = " закрыты"
            if pron == 5: verb = " закрыта"
        if ver == 3: verb = " использовать"
        if ver == 4: verb = " справлятся"
        if ver == 5: verb = " говорить"
        if ver == 6: verb = " знать"
        if ver == 7: verb = " брать"
        if ver == 8: verb = " видеть"
        if ver == 9: verb = " работать"

    # Настоящее
    if man == 4 or man == 5 or man == 6:
        if ver == 0:
            if pron == 0: verb = " люблю"
            if pron == 1: verb = " любищь"
            if pron == 2: verb = " любим"
            if pron == 3: verb = " любят"
            if pron == 4 or pron == 5: verb = " любит"
        if ver == 1:
            if pron == 0: verb = " нравлюсь"
            if pron == 1: verb = " нравишься"
            if pron == 2: verb = " нравимся"
            if pron == 3: verb = " нравятся"
            if pron == 4 or pron == 5: verb = " нравится"
        if ver == 2:
            if pron == 0: verb = " закрываю"
            if pron == 1: verb = " закрываешь"
            if pron == 2: verb = " закрываем"
            if pron == 3: verb = " закрывают"
            if pron == 4 or pron == 5: verb = " закрывает"
        if ver == 3:
            if pron == 0: verb = " использую"
            if pron == 1: verb = " используешь"
            if pron == 2: verb = " используем"
            if pron == 3: verb = " используют"
            if pron == 4 or pron == 5: verb = " использует"
        if ver == 4:
            if pron == 0: verb = " справляюсь"
            if pron == 1: verb = " справляешься"
            if pron == 2: verb = " справляемся"
            if pron == 3: verb = " справляются"
            if pron == 4 or pron == 5: verb = "справляется"
        if ver == 5:
            if pron == 0: verb = " говорю"
            if pron == 1: verb = " говоришь"
            if pron == 2: verb = " говорим"
            if pron == 3: verb = " говорят"
            if pron == 4 or pron == 5: verb = " говорит"
        if ver == 6:
            if pron == 0: verb = " знаю"
            if pron == 1: verb = " знаешь"
            if pron == 2: verb = " знаем"
            if pron == 3: verb = " знают"
            if pron == 4 or pron == 5: verb = " знает"
        if ver == 7:
            if pron == 0: verb = " беру"
            if pron == 1: verb = " берешь"
            if pron == 2: verb = " берем"
            if pron == 3: verb = " берут"
            if pron == 4 or pron == 5: verb = " берет"
        if ver == 8:
            if pron == 0: verb = " вижу"
            if pron == 1: verb = " видешь"
            if pron == 2: verb = " видим"
            if pron == 3: verb = " видят"
            if pron == 4 or pron == 5: verb = " видит"
        if ver == 9:
            if pron == 0: verb = " работаю"
            if pron == 1: verb = " работаешь"
            if pron == 2: verb = " работаем"
            if pron == 3: verb = " работают"
            if pron == 4 or pron == 5: verb = " работает"

    # Прошлое
    if man == 7 or man == 8 or man == 9:
        if ver == 0:
            if pron == 0 or pron == 1 or pron == 4: verb = " любил"
            if pron == 2 or pron == 3: verb = " любили"
            if pron == 5: verb = " любила"
        if ver == 1:
            if pron == 0 or pron == 1 or pron == 4: verb = " нравился"
            if pron == 2 or pron == 3: verb = " нравились"
            if pron == 5: verb = " нравилась"
        if ver == 2:
            if pron == 0 or pron == 1 or pron == 4: verb = " закрыл"
            if pron == 2 or pron == 3: verb = " закрыли"
            if pron == 5: verb = " закрыла"
        if ver == 3:
            if pron == 0 or pron == 1 or pron == 4: verb = " использовал"
            if pron == 2 or pron == 3: verb = " использовали"
            if pron == 5: verb = " использовала"
        if ver == 4:
            if pron == 0 or pron == 1 or pron == 4: verb = " справился"
            if pron == 2 or pron == 3: verb = " справились"
            if pron == 5: verb = " справилась"
        if ver == 5:
            if pron == 0 or pron == 1 or pron == 4: verb = " говорил"
            if pron == 2 or pron == 3: verb = " говорили"
            if pron == 5: verb = " говорила"
        if ver == 6:
            if pron == 0 or pron == 1 or pron == 4: verb = " знал"
            if pron == 2 or pron == 3: verb = " знали"
            if pron == 5: verb = " знала"
        if ver == 7:
            if pron == 0 or pron == 1 or pron == 4: verb = " брал"
            if pron == 2 or pron == 3: verb = " брали"
            if pron == 5: verb = " брала"
        if ver == 8:
            if pron == 0 or pron == 1 or pron == 4: verb = " видел"
            if pron == 2 or pron == 3: verb = " видели"
            if pron == 5: verb = " видела"
        if ver == 9:
            if pron == 0 or pron == 1 or pron == 4: verb = " работал"
            if pron == 2 or pron == 3: verb = " работали"
            if pron == 5: verb = " работала"
    return verb


def rus_text(man, ver, pron, pr):
    """Сборка текста вопроса
    :param man: main random num for table
    :param ver: random num for list verbs
    :param pron: random num for list pronouns
    :param pr: pronouns
    :return: print text"""
    if man == 1:
        if pron == 0: print(pr + "буду" + declension_russ(man, ver, pron) + "?")
        if pron == 1: print(pr + "будешь" + declension_russ(man, ver, pron) + "?")
        if pron == 2: print(pr + "будем" + declension_russ(man, ver, pron) + "?")
        if pron == 3: print(pr + "будут" + declension_russ(man, ver, pron) + "?")
        if pron == 4 or pron == 5: print(pr + "будет" + declension_russ(man, ver, pron) + "?")
    if man == 2:
        if pron == 0: print(pr + "буду" + declension_russ(man, ver, pron))
        if pron == 1: print(pr + "будешь" + declension_russ(man, ver, pron))
        if pron == 2: print(pr + "будем" + declension_russ(man, ver, pron))
        if pron == 3: print(pr + "будут" + declension_russ(man, ver, pron))
        if pron == 4 or pron == 5: print(pr + "будет" + declension_russ(man, ver, pron))
    if man == 3:
        if pron == 0: print(pr + "не буду" + declension_russ(man, ver, pron))
        if pron == 1: print(pr + "не будешь" + declension_russ(man, ver, pron))
        if pron == 2: print(pr + "не будем" + declension_russ(man, ver, pron))
        if pron == 3: print(pr + "не будут" + declension_russ(man, ver, pron))
        if pron == 4 or pron == 5: print(pr + "не будет" + declension_russ(man, ver, pron))
    if man == 4:
        print(pr + declension_russ(man, ver, pron) + "?")
    if man == 5:
        print(pr + declension_russ(man, ver, pron))
    if man == 6:
        print(pr + "не" + declension_russ(man, ver, pron))
    if man == 7:
        print(pr + declension_russ(man, ver, pron) + "?")
    if man == 8:
        print(pr + declension_russ(man, ver, pron))
    if man == 9:
        print(pr + "не" + declension_russ(man, ver, pron))


def main_table():
    right_ans, all_ans = 0, 0
    print("Что хочешь изучить?")
    rand_num_man = input("Будующее - 1 | Настоящее - 2 | Прошлое - 3 | Все - Enter \n")
    if rand_num_man == "1":
        range_man_from = 1
        range_man_to = 3
    elif rand_num_man == "2":
        range_man_from = 4
        range_man_to = 6
    elif rand_num_man == "3":
        range_man_from = 7
        range_man_to = 9
    else:
        range_man_from = 1
        range_man_to = 9

    while True:
        num_man = randint(range_man_from, range_man_to)
        num_pron = randint(0, 5)
        num_ver = randint(0, 9)
        p_eng = pronouns[0][num_pron]
        p_rus = pronouns[1][num_pron]
        verb_eng = verbs[0][num_ver]
        verb_eng_sp = verbs[1][num_ver]
        all_ans += 1

        # Меню
        os.system('cls||clear')
        print("/*/*/*/*/*/*/*/*/*/")
        print("\*\*\*\*\*\*\*\*\*\ \n")
        print("Глагол - " + verb_eng)
        print("Местоимение - " + p_eng)
        print("################### \n")

        # Главная матрица
        if num_man == 1:
            print("**Вопрос в будущем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print("Will " + p_eng + verb_eng + "?")

        if num_man == 2:
            print("**Утверждение в будущем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print(p_eng + "will" + verb_eng)

        if num_man == 3:
            print("**Отрицание в будущем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print(p_eng + "will not" + verb_eng)

        if num_man == 4:
            print("**Вопрос в настоящем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            if p_eng == "He " or p_eng == "She ":
                print("Does " + p_eng + verb_eng + "?")
            else:
                print("Do " + p_eng + verb_eng + "?")

        if num_man == 5:
            print("**Утверждение в настоящем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            if p_eng == "He " or p_eng == "She ":
                print(p_eng + verb_eng + "S")
            else:
                print(p_eng + verb_eng)

        if num_man == 6:
            print("**Отрицание в настоящем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            if p_eng == "He " or p_eng == "She ":
                print(p_eng + "does not" + verb_eng)
            else:
                print(p_eng + "don`t" + verb_eng)

        if num_man == 7:
            print("**Вопрос в прошедшем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print("Did " + p_eng + verb_eng + "?")

        if num_man == 8:
            print("**Утверждение в прошедшем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print(p_eng + verb_eng_sp)

        if num_man == 9:
            print("**Отрицание в прошедшем времени**")
            rus_text(num_man, num_ver, num_pron, p_rus)
            input('Узнать ответ')
            print(p_eng + "did not" + verb_eng)

        print("Число - " + str(num_man) + "\n\n" + "Ответил верно?")
        a = input('''Да "+"| Нет "enter"| Выход "q" ''')
        if a == "q":
            print("Правильных ответов - " + str(right_ans) + " из - " + str(all_ans))
            time.sleep(1.5)
            break
        elif a == "+":
            right_ans += 1
            print("Круто")
            time.sleep(0.7)
    os.system('cls||clear')
