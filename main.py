import os
import time
from rep_wor import rep_wor
from dictionary import dictionary
from main_table import main_table
from tobe_table import tobe_table

print(
    '''*~~~~~~~~~~~~~~~~~~~~~~~~~~~*
Welcome to the "English learn"
I hope you will like this
*~~~~~~~~~~~~~~~~~~~~~~~~~~~*'''
)

while True:
    ans = input('''
*Please select what you want study*
1 Main table (simple expressions) 
2 Table verb "to be"
3 Dictionary
4 Repeat other words
0 Exit/Quit
****************************
I am choosing... ''')

    if ans == "1":
        os.system('cls||clear')
        main_table()
    elif ans == "2":
        os.system('cls||clear')
        tobe_table()
    elif ans == "3":
        os.system('cls||clear')
        dictionary()
    elif ans == "4":
        os.system('cls||clear')
        rep_wor()
    elif ans == "0":
        os.system('cls||clear')
        print("\n\t Goodbye! ;)")
        time.sleep(1)
        break
    else:
        print("\n<!!!> Not Valid Choice Try again")
        time.sleep(1)
        os.system('cls||clear')
