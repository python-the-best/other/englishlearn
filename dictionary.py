import os
import time
from rep_wor import man_qes

pwd = os.path.dirname(os.path.abspath(__file__))  # path to the project


def dictionary():
    a, b = 0, 0
    diction = [[a], [b]]
    while True:
        os.system('cls||clear')
        ans_dic = input('''
Make your choice:
1 Learn words
2 Add words in Dictionary
3 Delete dictionary 
0 Exit/Quit
*Enter number: ''')
        if ans_dic == "1":
            if not os.path.isfile(pwd + "/Dictionary.txt"):
                print("\n<!!!> File is not exist")
                time.sleep(1)
                open(pwd + '/Dictionary.txt', 'w').close()
            elif os.stat(pwd + "/Dictionary.txt").st_size == 0:
                print("\n<!!!> You dictionary is empty!")
                time.sleep(1)
            else:
                my_file = open(pwd + "/Dictionary.txt", encoding='utf8')
                content = my_file.read()
                content_list = content.split("\n")
                for wor in content_list:
                    ind = wor.split(" : ")
                    for i in ind:
                        diction[a].append(i)
                        a += 1
                    a = 0
                    b += 1
                my_file.close()
                dic_edge = len(diction[0]) - 1
                man_qes(diction, 1, dic_edge)  # Call external function
                os.system('cls||clear')
        elif ans_dic == "2":
            print("~~~~~~~~~~~~~~~")
            word = input("Enter word: ")
            with open(pwd + "/Dictionary.txt") as file:
                if word in file.read():
                    print("\n<!!!> This word already exist in this Dictionary")
                    time.sleep(1)
                    continue
            transl = input("Enter translate: ")
            with open(pwd + '/Dictionary.txt', 'a+', encoding='utf8') as f:
                f.seek(0)
                data = f.read(100)
                if len(data) > 0:
                    f.write("\n")
                f.write(word + " : " + transl)
                f.close()
            os.system('cls||clear')
        elif ans_dic == "3":
            if input("\n\t<!!!> Are you sure? (Yes 'y') ") == "y":
                open(pwd + '/Dictionary.txt', 'w').close()
            os.system('cls||clear')
        elif ans_dic == "0":
            os.system('cls||clear')
            break
        else:
            print("\n<!!!> Not Valid Choice Try again")
            time.sleep(1)
            os.system('cls||clear')
    os.system('cls||clear')
