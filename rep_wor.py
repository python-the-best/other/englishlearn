import os
import time
from random import randint


ques = [
    ["What", "Who", "Where", "When", "Why", "How", "Which", "Whose", "Whom"],
    ["Что? Какой?", "Кто? Который?", "Где? Куда?", "Когда?", "Почему? Зачем?", "Как? Каким образом?", "Который? Какой?",
     "Чей? Чьё?", "Кого? Кому?"]]

adverb = [
    ["soon", "here", "there", "too", "almost", "partly", "often", "seldom", "above", "below", "even", "these"],
    ["скоро", "здесь", "там", "слишком", "почти", "частично", "часто", "редко", "наверху", "внизу", "даже", "эти"]]

iver = [
    ["become", "begin", "break", "buy", "choose", "come", "drink", "eat", "feel", "fly", "get", "give", "go", "have", "keep", "know", "leave", "lose", "make", "pay", "run", "say", "see", "sell", "sit", "sleep", "speak", "take", "tell", "think", "undo", "wake", "win", "write"],
    ["became", "began", "broke", "bought", "chose", "came", "drank", "ate", "felt", "flew", "got", "gave", "went", "had", "kept", "knew", "left", "lost", "made", "paid", "ran", "said", "saw", "sold", "sat", "slept", "spoke", "took", "told", "thought", "undid", "woke", "won", "wrote"],
    ["Становиться", "Начинать", "Ломать", "Покупать", "Выбирать", "Приходить", "Пить", "Есть", "Чувствовать", "Летать", "Получать", "Давать", "Идти", "Иметь", "Содержать", "Знать", "Оставлять", "Терять", "Производить", "Платить", "Бежать", "Говорить", "Видеть", "Продавать", "Сидеть", "Спать", "Говорить", "Брать, взять", "Рассказывать", "Думать", "Уничтожать, отменять", "Просыпаться", "Выигрывать", "Писать"]]


def man_qes(dic, start, end):
    """Генерирует диалог: вопрос - ответ
    :param dic:list with words
    :param start:start num for random gen
    :param end:stop num for random gen
    :return:dialog with user
    """
    while True:
        os.system('cls||clear')
        num_rand = randint(start, end)
        print(dic[0][num_rand])
        input("Показать ответ")
        print(dic[1][num_rand])
        if dic == iver:
            print("Перевод: " + dic[2][num_rand])
        if input("Продолжить? Enter | Выйти? q: ") == "q": break


def rep_wor():
    while True:
        ans_wor = input('''
Make your choice:
1 Repeat questions
2 Repeat adverb
3 Irregular Verbs 
0 Exit/Quit
*Enter number: ''')
        if ans_wor == "1":
            man_qes(ques, 0, 8)
            os.system('cls||clear')
        elif ans_wor == "2":
            man_qes(adverb, 0, 11)
            os.system('cls||clear')
        elif ans_wor == "3":
            man_qes(iver, 0, 33)
            os.system('cls||clear')
        elif ans_wor == "0":
            os.system('cls||clear')
            break
        else:
            print("\n<!!!> Not Valid Choice Try again")
            time.sleep(1)
            os.system('cls||clear')
    os.system('cls||clear')
